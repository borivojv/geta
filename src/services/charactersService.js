import Api from '@/services/api'

export default {
  fetchCharacters () {
    return Api().get('/character')
  }
}
